package de.sigmalab.siggi.steps;

import org.assertj.core.api.Assertions;

public class ValidationStep extends AbstractStep<AbstractStepContext> {

	private final Integer expectedResult;

	public ValidationStep(Integer expected) {
		this.expectedResult = expected;
	}

	@Override
	public void execute(AbstractStepContext context) throws StepExecutionException {
		Integer result = (Integer) context.get("result");
		try {
			Assertions.assertThat(result).isEqualTo(expectedResult);
		} catch (Throwable e) {
			throw new StepExecutionException("Result not expected " + result + ", should be " + expectedResult);
		}
	}
}
