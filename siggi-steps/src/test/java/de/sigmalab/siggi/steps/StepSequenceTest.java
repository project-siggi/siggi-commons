package de.sigmalab.siggi.steps;

import org.junit.Test;

/**
 * @author jbellmann
 */
public class StepSequenceTest {

	@Test
	public void simpleSequence() {
		SimpleStepContext ssc = new SimpleStepContext();
		ssc.put("s1", 12);
		ssc.put("s2", 13);
		StepSequence<AbstractStepContext> seq = new SimpleStepSequence();
		seq.add(new AddStep());
		seq.add(new SubStep(2));
		seq.add(new ValidationStep(Integer.valueOf(23)));

		seq.execute(ssc);
	}

	@Test(expected = StepExecutionException.class)
	public void simpleSequenceWithFailure() {
		SimpleStepContext ssc = new SimpleStepContext();
		ssc.put("s1", 12);
		ssc.put("s2", 13);
		StepSequence<AbstractStepContext> seq = new SimpleStepSequence();
		seq.add(new AddStep());
		seq.add(new SubStep(2));
		seq.add(new ValidationStep(Integer.valueOf(24)));

		seq.execute(ssc);
	}

}
