package de.sigmalab.siggi.steps;

/**
 * @author jbellmann
 */
public class AddStep extends AbstractStep<AbstractStepContext> {

	@Override
	public void execute(AbstractStepContext context) throws StepExecutionException {
		Integer s1 = (Integer) context.get("s1");
		Integer s2 = (Integer) context.get("s2");
		context.put("result", s1 + s2);
	}
}
