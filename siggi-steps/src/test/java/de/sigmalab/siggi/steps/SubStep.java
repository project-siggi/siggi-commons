package de.sigmalab.siggi.steps;

public class SubStep extends AbstractStep<AbstractStepContext> {

	private final Integer sub;

	public SubStep(Integer sub) {
		this.sub = sub;
	}

	@Override
	public void execute(AbstractStepContext context) throws StepExecutionException {
		Integer r = (Integer) context.get("result");

		context.put("result", r - sub);
	}

}
