package de.sigmalab.siggi.steps;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author jbellmann
 */
public abstract class AbstractStepContext implements StepContext {

	private final Map<String, Object> delegate = new HashMap<String, Object>();

	public AbstractStepContext() {
		//
	}

	public AbstractStepContext(AbstractStepContext other) {
		this.delegate.putAll(other);
	}

	@Override
	public int size() {
		return delegate.size();
	}

	@Override
	public boolean isEmpty() {
		return delegate.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return delegate.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return delegate.containsValue(value);
	}

	@Override
	public Object get(Object key) {
		return delegate.get(key);
	}

	@Override
	public Object put(String key, Object value) {
		return delegate.put(key, value);
	}

	@Override
	public String remove(Object key) {
		return remove(key);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
		delegate.putAll(m);
	}

	@Override
	public void clear() {
		delegate.clear();

	}

	@Override
	public Set<String> keySet() {
		return delegate.keySet();
	}

	@Override
	public Collection<Object> values() {
		return delegate.values();
	}

	@Override
	public Set<java.util.Map.Entry<String, Object>> entrySet() {
		return delegate.entrySet();
	}

}
