package de.sigmalab.siggi.steps;

import java.util.Map;

/**
 * @author jbellmann
 */
public interface StepContext extends Map<String, Object> {}
