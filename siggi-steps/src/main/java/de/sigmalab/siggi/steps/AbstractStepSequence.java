package de.sigmalab.siggi.steps;

import java.util.List;

import com.google.common.collect.Lists;

/**
 * @author jbellmann
 */
public class AbstractStepSequence<T extends StepContext> implements StepSequence<T> {

	private final List<Step<T>> steps = Lists.newArrayList();

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

	@Override
	public void execute(T context) throws StepExecutionException {
		for (Step<T> s : steps) {
			s.execute(context);
		}
	}

	@Override
	public void add(Step<T> step) {
		this.steps.add(step);
	}

}
