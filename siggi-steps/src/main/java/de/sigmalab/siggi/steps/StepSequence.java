package de.sigmalab.siggi.steps;

/**
 * @author jbellmann
 */
public interface StepSequence<T extends StepContext> extends Step<T> {

	void add(final Step<T> step);

}
