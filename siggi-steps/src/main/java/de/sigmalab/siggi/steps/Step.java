package de.sigmalab.siggi.steps;

/**
 * A Step is small 'unit of work'.
 * 
 * @author jbellmann
 */
public interface Step<T extends StepContext> {

	/**
	 * Name of Step.
	 *
	 * @return
	 */
	String getName();

	/**
	 * Execution of step with provided context.
	 *
	 * @param context
	 * @throws StepExecutionException
	 */
	void execute(final T context) throws StepExecutionException;
}
