package de.sigmalab.siggi.steps;

/**
 * @author jbellmann
 */
public abstract class AbstractStep<T extends StepContext> implements Step<T> {

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

}
