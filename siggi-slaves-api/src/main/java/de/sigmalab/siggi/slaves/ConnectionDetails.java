package de.sigmalab.siggi.slaves;

import com.google.common.base.MoreObjects;

/**
 * Information needed to connect to an {@link Slave}.
 * 
 * @author jbellmann
 */
public class ConnectionDetails {

	private String hostname;

	private int port;

	private String username;

	private String password;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("hostname", hostname).add("port", port).add("username", username)
				.add("passwort", "*******").toString();
	}

}
