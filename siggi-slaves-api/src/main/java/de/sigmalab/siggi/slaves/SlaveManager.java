package de.sigmalab.siggi.slaves;

/**
 * A {@link SlaveManager} is responsible to manage slaves.<br/>
 * Maybe provide 'static' {@link Slave}s from a list or spawn some cloud-slaves for every request.
 * 
 * @author jbellmann
 */
public interface SlaveManager {

	/**
	 * To request an {@link Slave} to build on.
	 * 
	 * @return a {@link Slave} with {@link ConnectionDetails}
	 */
	Slave requestSlave(String slaveHint);

	/**
	 * If a {@link Slave} is not needed anymore, release it.
	 */
	void releaseSlave(String slaveId);

}
