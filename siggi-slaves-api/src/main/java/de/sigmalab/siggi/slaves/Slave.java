package de.sigmalab.siggi.slaves;

/**
 * 
 * @author jbellmann
 *
 */
public abstract class Slave {
	
	private String id;
	
	public String getId(){
		return id;
	}

	public abstract ConnectionDetails getConnectionDetails();
}
