package de.sigmalab.slaves.aws;

import java.io.IOException;
import java.util.concurrent.Semaphore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;

import de.sigmalab.siggi.slaves.ConnectionDetails;
import de.sigmalab.siggi.slaves.Slave;
import de.sigmalab.siggi.slaves.SlaveManager;

/**
 * @author mrandi
 */
public class EC2SlaveManager implements SlaveManager {

	private final Logger log = LoggerFactory.getLogger(EC2SlaveManager.class);

	@Autowired private Environment environment;

	private AmazonEC2Client amazonEC2Client;

	private final Semaphore sem;

	public EC2SlaveManager() {
		this(10);
	}

	public EC2SlaveManager(int maxNumberOfSlaves) {
		AWSCredentials credentials = null;
		try {
			credentials = new PropertiesCredentials(getClass().getResourceAsStream("AwsCredentials.properties"));
		} catch (IOException e) {}

		AmazonEC2Client amazonEC2Client = new AmazonEC2Client(credentials);
		amazonEC2Client.setEndpoint("ec2.eu-west-1.amazonaws.com");
		this.sem = new Semaphore(maxNumberOfSlaves, true);
	}

	@Override
	public Slave requestSlave(final String imageId) {

		try {
			sem.acquire();
		} catch (InterruptedException e) {
			log.warn(e.getMessage(), e);
		}

		RunInstancesRequest runInstancesRequest = new RunInstancesRequest();

		runInstancesRequest.withImageId(imageId).withInstanceType("m1.small").withMinCount(1).withMaxCount(1)
				.withKeyName("my-key-pair").withSecurityGroups("team-security-group");

		RunInstancesResult runInstancesResult = amazonEC2Client.runInstances(runInstancesRequest);

		ConnectionDetails connectionDetails = new ConnectionDetails();
		connectionDetails.setPort(environment.getProperty("port", Integer.class));
		connectionDetails.setUsername(environment.getProperty("username", String.class));
		connectionDetails.setPassword(environment.getProperty("password", String.class));

		Instance myInstance = runInstancesResult.getReservation().getInstances().get(0);

		if (environment.getProperty("usePublicIp", Boolean.class)) {
			String publicIpAddress = myInstance.getPublicIpAddress();
			connectionDetails.setHostname(publicIpAddress);
		} else {
			String privateIpAddress = myInstance.getPrivateIpAddress();
			connectionDetails.setHostname(privateIpAddress);
		}

		EC2Slave slave = new EC2Slave(myInstance.getInstanceId(), connectionDetails);

		return slave;
	}

	/**
	 * slave id is also the instance id.
	 */
	@Override
	public void releaseSlave(final String slaveId) {
		try {

			StopInstancesRequest stopInstanceRequest = new StopInstancesRequest();
			stopInstanceRequest.withInstanceIds(slaveId); // withForce(true) ???

			StopInstancesResult stopInstancesResult = amazonEC2Client.stopInstances(stopInstanceRequest);
			// stopInstancesResult.getStoppingInstances().get(0).getCurrentState();
		} finally {
			// maybe this is not the correct way
			sem.release();
		}
	}
}
