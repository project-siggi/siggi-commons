package de.sigmalab.slaves.aws;

import java.util.Objects;

import de.sigmalab.siggi.slaves.ConnectionDetails;
import de.sigmalab.siggi.slaves.Slave;

/**
 * @author mrandi
 */
class EC2Slave extends Slave {

	private String id;

	private ConnectionDetails connectionDetails;

	EC2Slave(final String id, final ConnectionDetails connectionDetails) {
		Objects.requireNonNull(connectionDetails, "ConnectionDetails should not be null");
		this.id = id;
		this.connectionDetails = connectionDetails;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public ConnectionDetails getConnectionDetails() {
		return connectionDetails;
	}
}
