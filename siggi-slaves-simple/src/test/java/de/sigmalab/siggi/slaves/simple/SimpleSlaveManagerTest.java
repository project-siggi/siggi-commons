package de.sigmalab.siggi.slaves.simple;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import com.google.common.collect.Lists;

import de.sigmalab.siggi.slaves.ConnectionDetails;
import de.sigmalab.siggi.slaves.Slave;
import de.sigmalab.slaves.simple.SimpleSlaveManager;

public class SimpleSlaveManagerTest {

	@Test
	public void slaveManagerCreation() {
		SimpleSlaveManager ssm = new SimpleSlaveManager(getConnectionDetails());
		Slave s = ssm.requestSlave("SimpleSlaveManager does not care about slaveHint");
		Assertions.assertThat(s).isNotNull();
		Assertions.assertThat(s.getId()).isNotNull();
		Assertions.assertThat(s.getId()).isNotEmpty();
		Assertions.assertThat(s.getConnectionDetails()).isNotNull();
	}

	@Test(expected = IllegalArgumentException.class)
	public void noSimpleSlaveManagerCreationWithoutConnectionDetails() {
		new SimpleSlaveManager(Lists.newArrayList());
	}

	protected List<ConnectionDetails> getConnectionDetails() {
		List<ConnectionDetails> connectionDetails = Lists.newArrayList();
		ConnectionDetails cd = new ConnectionDetails();
		cd.setHostname("localhost");
		cd.setUsername("siggi");
		cd.setPort(22);
		cd.setPassword("geheim");
		connectionDetails.add(cd);
		return connectionDetails;
	}

}
