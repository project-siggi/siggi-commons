package de.sigmalab.slaves.simple;

import java.util.Objects;

import de.sigmalab.siggi.slaves.ConnectionDetails;
import de.sigmalab.siggi.slaves.Slave;

/**
 * A simple slave implementation.
 * 
 * @author jbellmann
 */
class SimpleSlave extends Slave {

	private String id;

	private ConnectionDetails connectionDetails;

	SimpleSlave(final int id, final ConnectionDetails connectionDetails) {
		Objects.requireNonNull(connectionDetails, "ConnectionDetails should not be null");
		this.id = String.valueOf(id);
		this.connectionDetails = connectionDetails;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public ConnectionDetails getConnectionDetails() {
		return connectionDetails;
	}
}
