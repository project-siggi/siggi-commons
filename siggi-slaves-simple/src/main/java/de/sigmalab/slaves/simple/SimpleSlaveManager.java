package de.sigmalab.slaves.simple;

import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Lists.newArrayList;
import static java.lang.Integer.valueOf;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.google.common.collect.Maps;

import de.sigmalab.siggi.slaves.ConnectionDetails;
import de.sigmalab.siggi.slaves.Slave;
import de.sigmalab.siggi.slaves.SlaveManager;

/**
 * Simple implementation of {@link SlaveManager}.
 * 
 * @author jbellmann
 */
public class SimpleSlaveManager implements SlaveManager {

	private final Logger log = LoggerFactory.getLogger(SimpleSlaveManager.class);

	private final Semaphore sem;

	private final SimpleSlave[] slaves;

	private Map<String, SimpleSlave> workingSlaves = Maps.newHashMap();

	public SimpleSlaveManager(List<ConnectionDetails> slaveConnections) {
		Assert.notNull(slaveConnections, "List of 'StaticConnection's should not be empty");

		List<ConnectionDetails> nonNullConnections = newArrayList(filter(slaveConnections, notNull()));
		Assert.notEmpty(nonNullConnections, "List of 'StaticConnection's should not be empty");

		this.slaves = new SimpleSlave[nonNullConnections.size()];
		this.sem = new Semaphore(this.slaves.length, true);

		int index = 0;
		for (ConnectionDetails con : nonNullConnections) {
			this.slaves[index] = new SimpleSlave(index, con);
			index++;
		}

		log.info("{} Slave(s) initialized", index);
	}

	@Override
	public Slave requestSlave(String slaveHint) {

		// we do not care about the 'slaveHint', it is only relevant for dynamic hosts (docker, aws, ... )

		try {
			this.sem.acquire();
		} catch (InterruptedException e) {
			//
		}

		synchronized (this) {
			SimpleSlave slave = null;
			for (int i = 0; i < this.slaves.length; i++) {
				if (this.slaves[i] != null) {
					slave = this.slaves[i];
					this.workingSlaves.put(String.valueOf(i), slave);
					this.slaves[i] = null;

					break;
				}
			}

			return slave;
		}
	}

	@Override
	public void releaseSlave(String slaveId) {

		if (this.slaves[valueOf(slaveId)] != null) {
			throw new IllegalStateException("");
		} else {
			this.slaves[valueOf(slaveId)] = workingSlaves.remove(slaveId);
			this.sem.release();
		}
	}

}
